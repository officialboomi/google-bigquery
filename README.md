# Google BigQuery Connector

Use the Google BigQuery connector to stream and insert incremental structured data into Google BigQuery.

After streaming and inserting your data, you can do the following:

- Run a job to query against the data in Google BigQuery.
- Retrieve the results of a query against the data from the Run Job operation.
- Update a Google BigQuery table and view.

Google BigQuery is a fully-managed enterprise analytics data warehouse that you can use to store all of your data. You can run SQL queries to analyze terabytes of data in seconds and petabytes in minutes to find useful insights. The data is analyzed in real-time. You can easily load, process, and make interactive visualizations of your data.

More information can be found at https://help.boomi.com/bundle/connectors/page/r-atm-Google_BigQuery_connector.html
